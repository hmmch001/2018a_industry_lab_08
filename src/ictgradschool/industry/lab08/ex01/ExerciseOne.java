package ictgradschool.industry.lab08.ex01;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ExerciseOne {

    public void start() {

        printNumEsWithFileReader();

        printNumEsWithBufferedReader();

    }

    private void printNumEsWithFileReader() {

        int numE = 0;
        int total = 0;
        FileReader fR;
        int x = 0;



        try {
            fR = new FileReader("input2.txt");

            while(x != -1) {
                 x = fR.read();
                if (x == 'e' || x == 'E') {
                    numE = numE + 1;
                }
                total = total + 1;
            }
        }catch(IOException e) {
        }


        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    private void printNumEsWithBufferedReader() {

        int numE = 0;
        int total = 0;
        int x = 0;

        try(BufferedReader reader = new BufferedReader( new FileReader("input2.txt"))){
            while(x != -1) {
                x = reader.read();
                if (x == 'e' || x == 'E') {
                    numE = numE + 1;
                }
                total = total + 1;
            }
        }catch(IOException e) {
        }

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    public static void main(String[] args) {
        new ExerciseOne().start();
    }

}
