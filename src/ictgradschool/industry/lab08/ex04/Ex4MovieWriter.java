package ictgradschool.industry.lab08.ex04;

import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieWriter;

import java.io.*;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieWriter extends MovieWriter {

    @Override
    protected void saveMovies(String fileName, Movie[] films) {
        File myFile = new File(fileName+".csv");
        try (PrintWriter writer = new PrintWriter(new FileWriter(myFile))) {
            for (int i = 0; i < films.length; i++) {
                String movie = (films[i].getName() + "," + films[i].getYear() + "," + films[i].getLengthInMinutes() + "," + films[i].getDirector());
                writer.println(movie);
            }
        } catch (IOException e) {
            System.out.println("error");
        }
    }



    public static void main(String[] args) {
        new Ex4MovieWriter().start();
    }

}
