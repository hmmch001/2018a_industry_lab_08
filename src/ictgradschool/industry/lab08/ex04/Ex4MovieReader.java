package ictgradschool.industry.lab08.ex04;

import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieReader;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieReader extends MovieReader {

    @Override
    protected Movie[] loadMovies(String fileName) {

        File myFile = new File(fileName);
        Movie [] films = new Movie [19];
        String movie;
        String name;
        int year;
        int length;
        String director;
        try (Scanner scanner = new Scanner(myFile)) {
            for(int i = 0; i < films.length; i ++){
                movie = scanner.nextLine();
                String [] arrOfmovie = movie.split(",");
                name = arrOfmovie [0];
                year = Integer.parseInt(arrOfmovie [1]);
                length = Integer.parseInt(arrOfmovie [2]);
                director = arrOfmovie [3];
                films [i] = new Movie ( name, year, length, director);

            }


        } catch (IOException e) {
            System.out.println("Error");
        }



        return films;
    }

    public static void main(String[] args) {
        new Ex4MovieReader().start();
    }
}
