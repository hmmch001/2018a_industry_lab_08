package ictgradschool.industry.lab08.ex02;

import ictgradschool.Keyboard;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class MyScanner {

    public void start() {
        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();
        File newFile = new File(fileName);

        try (Scanner scanner = new Scanner(newFile)) {

            while (scanner.hasNextLine()) {

                System.out.println(scanner.nextLine());
            }


        } catch (IOException e) {
            System.out.println("Error" );
        }

    }

    public static void main(String[] args) {
        new MyScanner().start();
    }
}
